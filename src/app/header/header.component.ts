import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public demoEmail: number = 34;
  public demoNoti: number = 11;

  constructor() { }

  ngOnInit(): void {
  }

}
